# How to use

This library is module for https://etherspy.io website.

See example below or demo project: https://gitlab.com/etherspy-public/etherspy-react-example

```js
import EtherSpy from 'etherspy-lib';

const initES = async () => {
  const es = new EtherSpy();
  es.valueTransferUpdate(data => console.log('1) valueTransferUpdate', data));
  es.tokenTransferUpdate(data => console.log('2) tokenTransferUpdate', data));
  const socketId = await es.connect();
  console.log('Connected to ES node!', socketId);

  // Bitance wallet
  es.subscribeValueTransfer('0xD551234Ae421e3BCBA99A0Da6d736074f22192FF', 'FromTo');

  // TRX Token
  es.subscribeTokenTransfer('0xf230b790E05390FC8295F4d3F60332c93BEd42e2', {from: '*', to: '*'});

  setTimeout(() => {
    // Bitance wallet
    es.unSubscribeValueTransfer('0xD551234Ae421e3BCBA99A0Da6d736074f22192FF');

    // TRX Token
    es.unSubscribeTokenTransfer('0xf230b790E05390FC8295F4d3F60332c93BEd42e2');
  }, 10 * 1000);
}

// and

initES();

```
