const socketio = require('socket.io-client');

class EtherSpy {
  constructor(data) {
    const url = (data && data.devMode) ? 'http://localhost:16001' : 'https://es-mainnode.etherspy.io';
    this.socket = socketio(url, {
      path: '/ws-esmainnode',
      transports: ['websocket'],
      autoConnect: false,
    });

    this.connected = false;
    this.socketId = null;

    // Bind functions
    this.connect = this.connect.bind(this);
    this.subscribeTokenTransfer = this.subscribeTokenTransfer.bind(this);
    this.unSubscribeTokenTransfer = this.unSubscribeTokenTransfer.bind(this);
    this.tokenTransferUpdate = this.tokenTransferUpdate.bind(this);
    this.subscribeValueTransfer = this.subscribeValueTransfer.bind(this);
    this.unSubscribeValueTransfer = this.unSubscribeValueTransfer.bind(this);
    this.valueTransferUpdate = this.valueTransferUpdate.bind(this);
  }

  connect() {
    return new Promise((resove, reject) => {
      this.socket.on('connect', () => {
        this.connected = true;
        this.socketId = this.socket.id;
        resove(this.socketId)
      });
      this.socket.on('tokenTransferUpdate', message => this.tokenTransferUpdateFn(message));
      this.socket.on('valueTransferUpdate', message => this.valueTransferUpdateFn(message));
      this.socket.open();
    });
  }

  subscribeTokenTransfer(address, filter) {
    if (this.connected === false)
      throw new Error('Please execute method .connect before subscribe');
    
    if (!this.tokenTransferUpdateFn)
      throw new Error('Please execute method .tokenTransferUpdate with proper callback function before subscribe');

    if (!address)
      throw new Error('Please input address');

    if (!filter)
      throw new Error('Please input filter');

    const obj = {
      contractAddress: address,
        filter: {
          from: filter.from,
          to: filter.to,
        }
    };
    this.socket.emit('subscribeTokenTransfer', obj);
  }

  unSubscribeTokenTransfer(address) {
    if (!address)
      throw new Error('Please input address');

    this.socket.emit('unSubscribeTokenTransfer', address);
  }

  tokenTransferUpdate(callback) {
    if (!callback)
      throw new Error('Please provide a callback function');

    this.tokenTransferUpdateFn = callback;
  }

  subscribeValueTransfer(walletAddress, type) {
    if (this.connected === false)
      throw new Error('Please execute method .connect before subscribe');

    if (!this.valueTransferUpdateFn)
      throw new Error('Please execute method .valueTransferUpdate with proper callback function before subscribe');

    if (!walletAddress)
      throw new Error('Please input wallet address');

    if (!type)
      throw new Error('Please input type');

    const obj = {
      walletAddress,
      type
    };
    this.socket.emit('subscribeValueTransfer', obj);
  }

  unSubscribeValueTransfer(walletAddress) {
    if (!walletAddress)
      throw new Error('Please input walletAddress');

   this.socket.emit('unSubscribeValueTransfer', walletAddress);
  }

  valueTransferUpdate(callback) {
    if (!callback)
      throw new Error('Please provide a callback function');

    this.valueTransferUpdateFn = callback;
  }

}

module.exports = EtherSpy;
